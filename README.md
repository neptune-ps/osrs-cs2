# osrs-cs2

This repo is meant to be used with [ClientScriptCompilerApplication]. The
scripts in `src/osrs` are taken from the output of [RuneStar] decompiler
with some slight modifications to fix parse errors.

You can build this project by running `compiler/bin/cs2 [neptune.toml]`. For
more information on the compiler look at the [Neptune] repo.

This project is meant to be used as an example setup and as a test of the
compiler and does not include every symbol that might exist, only those that
are required.

## Structure

- `/src/commands`: Contains the signatures of most commands.
- `/src/osrs`: Contains the output of [RuneStar] decompiler.
- `/symbols`: Contains all symbols needed to compile all existing OSRS scripts

[Neptune]: https://gitlab.com/neptune-ps/neptune

[ClientScriptCompilerApplication]: https://gitlab.com/neptune-ps/neptune/-/blob/master/clientscript-compiler/src/main/kotlin/me/filby/neptune/clientscript/compiler/ClientScriptCompilerApplication.kt

[RuneStar]: https://github.com/Joshua-F/cs2
